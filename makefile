LY_FILES=$(shell ls *.ly)
PDF_FILES=$(LY_FILES:.ly=.pdf)

.PHONY: all
all: $(PDF_FILES)

%.pdf: %.ly
	lilypond "$<"

clean:
	rm *.pdf
