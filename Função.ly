\version "2.20.0"
\include "defaults.ly"

\header {
  title = "Função"
  composer = "Elomar"
}

\score {
  <<
    \new ChordNames \chordmode {
      \set chordChanges = ##t
      \time 4/4
      \partial 4. s4. |
      f1:m |
      c:m |
      c2:m f2:m |
      c1:m |
      f1:m |
      c:m |
      c2:m g2:7 |
      c1: m
      s8 aes8~ aes2. |
      es1 |
      \repeat unfold 3 { aes1 | es |}
      aes2:m bes:7 |
      es:m c:m |
      f:m g:7 |
      c1:m |
      f2:m aes |
      g:7 c:m |
      \repeat unfold 2 { aes1 | es |}
      aes2:m bes:7 |
      es:m c4:m f4:m |
      c2:m g:7 |
      c2.:m f4:m |
      c2:m g:7 |
      \time 2/4 c2:m \time 4/4 |
      des1 |
      es |
      des |
      es |
      bes:7 |
      c4.:m f8:m~ f2:m |
      \time 2/4 f2:m \time 4/4 |
      f1:m |
      f:m |
      f:m |
      f2:m c:m |
      c1:m |
      c:m |
      g:7 |
      c:m |
    }
    \new Staff {
      \key c \minor
      \time 4/4
      \set Timing.beamExceptions = #'()
      \set Timing.beatStructure = 1,1,1,1

      \new Voice = "intro" \relative c'' {
        \partial 4. c8 aes f |
        c'8. aes16 f8 c'~ c c aes f |
        c2 r8 c es g |
        \tuplet 3/2 { bes4 bes bes } \tuplet 3/2 { aes bes c } |
        r2 r8 c8 aes f |
        c'8. aes16 f8 c'~ c c aes f |
        c2 r8 c es g |
        \tuplet 3/2 { bes4 bes bes } g2 |
        c,2 r2 |
      }

      \repeat volta 2 {
        \new Voice = "melodia" \relative c'' {
          r8 c2. bes8 |
          bes2 r8 bes g bes |
          c c r aes c aes c bes~ |
          bes2 bes8 bes g bes |
          c c aes c aes c~ c bes~ |
          bes4. bes8 bes16 bes8 bes16 bes8 c~ |
          c4. aes8 \tuplet 3/2 { c4 es es } |
          bes2 bes8 bes ces bes |
          aes aes bes aes bes aes bes ges~ |
          ges es r g~ g4 ees8 g |
          f4 r8 g~ g g f g |
          es c r4. ees8 c es |
          f f f es c4. f8 |
          ees16 ees8 ees16 d8 c r4 ees8 c |
          aes' g aes bes aes es f g~ |
          g4 r4. ees8 es es |
          \tuplet 3/2 {c'4 c bes } \tuplet 3/2 { c bes c } |
          bes4 r \tuplet 3/2 { bes ces bes } |
          aes4 aes8 aes aes16 aes8 aes16 bes8 ges~ |
          ges es r f g g c g |
          es es r f g d~ d c~ |
          c g'~ g g~ g g c g |
          es es r f g d~ d c~ |
          \time 2/4 c2^\markup{\italic{fine}} \time 4/4
          f8 f f f16 f~ f4 r8 g~ |
          g es es es es es r f~ |
          f f f f16 f~ f4 r8 g~ |
          g es es16 es es8~ es es es es |
          bes' bes bes16 aes bes8~ bes bes bes aes |
          g g16 g g8 f8~ f4 r |
        }
        <<
          \relative c' { \voiceOne
            \time 2/4 r8 f aes c \time 4/4
            d2 des |
            c8. aes16 f8 c'~ c f, aes c |
            es4 des8 c~ c bes aes bes |
            c8. aes16 f8 c' r c, es g |
            <es a>2 <d aes'> |
            g8. es16 c8 g' r c, es g |
            bes4 aes8 g~ g f es d |
            c1 |
          }

          \new Voice \relative c' { \voiceTwo
            \time 2/4 s2 \time 4/4
            r8 f aes f r f aes f |
            s1 |
            aes4 aes8 aes~ aes4 s4
            s1 |
            c,8[ c c c] c[ c c c] |
          }
        >>
      }
    }

    \new Lyrics \lyricsto "melodia" {
      vem Jo -- ão
      trais as vi -- o -- la si -- gu -- ro na mão
      pe -- ga_a man -- du -- ré -- ba a -- ti -- ça_os ti -- ção
      car -- re -- ga pro ter -- rêro os ban -- co_e_as
        ca -- dêra
      e cha -- ma_as mi -- ni -- na prá ro -- dá o ba -- ião __ _
      nós dois sen -- tado jun -- to da fo -- gue -- ra
      va -- mo fa -- zê a nos -- sa brin -- ca -- dê -- ra e
        can -- tar
      a li -- jê -- ra, mo -- da de la -- va -- ção
      em ho -- me -- na -- ge ao nos -- so São João
      e prá_a -- ca -- bá com_a sau -- da -- de
        ma -- ta -- dê -- ra
      vo -- cê can -- ta li -- jê -- ra, can -- to moi -- rão
      vo -- cê can -- ta li -- jê -- ra, can -- to moi -- rão
      ai meu São Jo -- ão, lá da a -- li -- gri -- a
      ai meu São Jo -- ão, lá da a -- li -- gri -- a
      a sau -- da -- de ca -- da di -- a mais me doi no
        co -- ra -- ção
    }
    \new Lyrics \lyricsto "melodia" {
      vem Jo -- ão
      va -- mos_meu bi -- chin _ can -- tá o moi -- rão
      tem um bicho ro -- e -- no o meu co -- ra -- ção
      cua -- no_eu e -- ra mi -- nino a vi -- da_e -- ra manêra
      não_pen -- sa -- va na vi -- da jun -- to da
        fo -- guê -- ra
      brin -- cando com_os ir -- mão _ a noi -- te_in -- tê -- ra
      sem me dá qui_ês -- se tem -- po bom ha -- ve -- ra
        de pas -- sá
      e_a sau -- da -- de me che -- gá es -- sa fera
      quem pen -- sar que_ês -- se bi -- cho_é da ci -- dade
      s'inga -- na_a sau -- da -- de nas -- ceu cá no
        ser -- tão __ _
      na bê -- _ -- ra da fo -- guê -- ra de São João
      na bê -- ra da fo -- guê -- ra de São Jo -- ão
    }
  >>
}


%{


%}

