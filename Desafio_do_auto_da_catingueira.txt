G
Senhores donos da casa, o cantadô pede licença

Pra puxar viola rasa, aqui na vossa presença

Venho das banda do norte
                    C
Cum pirmissão da sentença

Cumpri minha sina forte
                   A
Já por muitos con'icida

Buscando a I'lusão da vida
               B
Ou o cutelo da morte
                  D
E das duas a prifirida
       B     A     E
a que me mandar a sorte

(G D E G D E)

E
Já que nunciei quem sou

Deixo meu convite feito

Pra qualqué dos cantadô

Dos que se dá por respeito

Que aqui por acaso teja

Nessa função de alegria

E pra que todos me veja

Puxo alto a cantoria

nessa viola de peleja

Que quando num mata aleja cantadô de arrelia

G
Só na escada de uma igreja

Labutei cua duza um dia

Cinco morreram de inveja

Três de avexo e um de agonia

Matei os bicho cum mote

Que já me deu três mulé

É a história dum cassote

Cum quati e com saqué

O cassote com o pote

Coou pro quati um café

D                   A
Iantes ofereceu o lote
C              G
Num saco pro saqué
    Bb          F
O saqué secou o pote
         Ab             Eb
Deixou o quati só com a fé
G                    D
De que dentro do tal pote
     E             B
Inda tinha algum café
     Bb             F
E xispô sambando um xote
    F#          C#
O enxavido do saqué
       Ebm          Bb
Qui cuati quá qui cassote
       G             D
Boto o bico e bato um bote
      E             B
O que é que o saqué quer?

G
Iantes porém aviso

Sô malvado, não aliso
C
Triste ou feliz é o cantadô
                          A
Que eu apanhar pra dar o castigo
                    B
Apois quem canta comigo
      A        D     E
Sai defunto ou sai dotô

(D Bm D E D)

D
Sô cantador chegante, me adesculpe o tratamento

Nessa hora nesse instante, mermo aqui nesse momento
                        Bm
Com um canto tão significante
    D              Bm
Sem fama sem atrevimento
      D
E num é muito falante
                      E
nem de muito conhecimento
D
Mas pra títulos e valentia

Só traz u'a viola na mão

Falta ilustre companheiro
                     G
Marcar o lugar da porfia
                 D
Se lá fora no terreiro

Ou aqui mémo no salão

(Eb Ab Bb Eb)
(Ab Bb Eb)

Eb
Vamo logo mano à obra
         Cm        Eb
Deixe as bestas de lado

Que a luma já fez manobra
       Cm         Eb
No seu canto alumiado

Vosmicê que sois daqui
       Cm          Eb
Vai deixando espiricado
                 F
As roda dos cantori

E que lhe é mais agradado
Eb
Se vamo cantar o moirão

O martelo ou a tirana

Ou a ligeira sussuarana

Parcela de mutirão

Ou entonce, ao invés

A obra de nove pés

De oito, sete, ou seis

Ou se dez pés, um quadrão

Vamo logo mano à obra

Deixe essas coisa de lado

Vamo cantar no salão
                    Ab
Tô mais riuna que a cobra
                     Bb
Que traz o rabo encravado
                Eb
Envenenado o ferrão

(Ab Bb Eb)
(Ab Bb Eb)

F                     Bb
Apois sim, tá certo: Vamo
      F            C
Cantá qualqué cantoria
    F                 Am
Brinquei-lhe em minha acamo
      Bb          F
Pra rodá a sabedoria

Vamo cantar, meu amigo
     Dm            F
As moda que for chegando

Num córreno assim o perigo
       Dm           F
Que tá sempre esp'ricando
                   G
P'esse povo que eu digo
                F
Enducado me escutano
                       Fm
A'pois pra entender parcela
     Bm           Fm
Martelo ou coco tirano

Tem que bater mil cancela
     Eb             Ab
Na estrada dos desengano
      Fm
E ainda púrriba tem
      Bm               Fm
Que saber, sofrer, esperar

Memo sabendo que não vêm
   Eb             Ab
As coisa do seu sonhá

Na estrada dos desengano
                    Fm
Andei de noite e de dia

A'pois sim, tá certo: Vamo

Cantá qualqué cantoria

      Bbm
Na estrada dos desengano

Andei de noite e de dia

Inludido percurando
                    Ab
Aprendê o que num sabia

Quando eu era moço, um dia

Risolvi sair andando

Pula estrada da alegria
               Bb
A alegria percurando

Curri doido, atrás dela
                 C
Entrou ano, saiu ano
                    Fm
Bati mais de mil cancela

Na estrada dos desengano

Bati mais de mil cancela

Na estrada dos desengano


(Cm)

Cm
Todo cantadô errante trás nos peito

Uma mazela nas alma lua minguante estrada
              Ebm   Cm
E o som de cancela
Cm
Todo cantadô errante trás nos peito

Uma mazela nas alma lua minguante estrada
              Ebm
E o som de cancela, ai
B                  C#
fonte que ficou distante
      Ebm         Bbm    Ebm
Que matava a sede dela
    Abm             Ebm
E o coração mais discrente
    Abm          Ebm
Dos amor da catingueira
                   Cm
Ai o amor é uma serepente
     Fm            Eb
Esse bicho morte a gente
     Ab   Eb Bb  Eb Bb Eb
Vamo pois cantar parcela
Cm/Bb Cm  Cm/Bb Cm
daindá,   daindá

G7
Eu sou cantador de cocô

Eu não canto parcela

Parcela feiticeira

Eu corro às legua dela, ah, ah
Bb
Chegando num lugar

Adonde teja ela
   Gm
Eu vo me adisculpano
  C          Gm
E dano nas canela
Gm/F  Gm  Gm/F  Gm  Gm/F  Gm
daindá,   daindá,   daindá

Dm
Conheci um cantadô distimido e valente

Que mangava dos amor e zombava a fé dos crente
F
Mas um dia ele topou nos batente dua jinela
Dm                                     C
Cum o bicho do amor mucamba pomba e donzela
         Eb Bb                            Gº   Gm
E o cantadô aos pouco foi se paixonano pruela
Eb                 C        D               Gm
Té que um dia ficô louco de tanto cantar parcela
                   F                           Gm
E hoje véve pela estrada, resmungando que a culpada

Foi a mucamba da janela
Gm/F  Gm  Gm/F  Gm  Gm/F  Gm
daindá,   daindá,   daindá


G7
Eu sou cantadô de coco

Apois quem canta parcela
        Eb    D7      Gm
Corre o risco são francisco

Morre doido cantan'ela
Gm/F  Gm  Gm/F  Gm  Gm/F  Gm
daindá,   daindá,   daindá
