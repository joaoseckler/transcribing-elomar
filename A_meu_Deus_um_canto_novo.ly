\version "2.20.0"

\include "defaults.ly"

\header {
  title = "A meu Deus um canto novo"
  composer = "Elomar"
}

melody = \relative c {
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1,1
  \improvisationOn
  \partial 4 ees8 ees | \repeat volta 3 { bes2 d4 | a2 c4 |
    g2 d4 | d2 \improvisationOff <d'' fis a>8 <d g b> |
    <fis a d>2 c'16 b g8 | <d fis a>2 <d fis>16 g <c, e> g' |
    <a, d fis>2 \improvisationOn a4 | d2 a4 | d2 \improvisationOff

    \new Voice = "melody" {
      c'8 b | a a fis a fis e | fis fis d d e fis |
      a a g g fis fis | e4 a\fermata e8 e |
      f f a a g g     | d d~ d d16 d ees8 d |
      bes bes bes c d d | a a a c c c |
      b b g g d'16 e d cis | d2 d16 d fis fis |
      a8 a a a bes f | a g f g a f | c' c c c b g |
      a2.\fermata | r4 r16 a16 c c bes bes a a |
      f8 f \tuplet 3/2 { r8 f a } \tuplet 3/2 { g8 f g } |
      d8 d d d ees d | bes bes bes c d d | a a a a c c |
      b b g g d'16 e d cis | d2.
    }
  }
}

crd = \chordmode {
  \partial 4 ees4 | \repeat volta 3 { bes2 d4:m | a2:m c4 | g2 d4 |
    d2 d8 g | d2. | d | d2 a4:7 | d2 a4:7 | d2.
     d2. | b:m | d4/a g/a d/a | e:7 a:7 c |
    f2  g4 | d2:m ees4 | bes2 d4:m | a2:m c4 |
    g2 d4 | d2. | fis2:m bes4 | f2. | a2:m g4/b |
    a2. | s2 bes4 | f2 g4 | d2:m ees4 | bes2 d4:m | a2:m c4 |
    g2 d4 | e2 \parenthesize ees4
  }
}

letraa = \lyricmode
{
  bem de lon -- ge na gran -- de vi -- a -- gem
  so -- bre -- car -- re -- ga -- do pa -- ro_a des -- can -- sar __ _

  e -- mer -- gi de pa -- ra -- gens ci -- ga -- nas pe -- las
  mãos de_El -- mana san -- tas co -- mo a luz em si -- lên -- cio
  con -- tem -- plo_en -- tão mais na -- da_a re -- ve -- lar

  fa -- di -- ga -- do_e far -- to de cla -- mar às pe -- dras
  de_en -- si -- nar jus -- ti -- ça_ao mun -- do pe -- ca -- dor

  oh lu -- a no -- va quem me de -- ra eu me_en -- con -- trar
  com el -- a no pis -- pei de tu -- do na qua -- dra
  per -- di -- da na ma -- nhã da_es -- tra -- da_e co -- me -- çar
  tu -- do de novo
}

letrab= \lyricmode
{
  to -- pei_em cer -- ta_al -- tu -- ra da jor -- na -- da
  com um que nem ti -- nha per -- nas pa -- ra_an -- dar __ _

  co -- mo -- veu\-me_em gran -- de com -- pai -- xão
  vol -- ta -- no_o_o -- lhar para_os céus
  re -- co -- men -- dou\-me_ao Deus
  Se -- nhor de to -- dos nós ro -- gan -- do na -- da me fal -- tar

  res -- fri -- an -- do_o_a -- mor a fé e_a ca -- ri -- da -- de
  ve -- jo_o se -- me -- lhan -- te_en -- trar em con -- fu -- são

  oh lu -- a no -- va quem me de -- ra eu me_en -- con -- trar
  com el -- a no pis -- pei de tu -- do na qua -- dra
  per -- di -- da na ma -- nhã da_es -- tra -- da_e co -- me -- çar
  tu -- do de novo
}

letrac = \lyricmode
{
  bô -- as no -- vas de ple -- na_a -- le -- gri -- a pas -- sa -- ram
  dois di -- as da res -- su -- rei -- ção __ _

  re -- ful -- gi -- da_u -- ma be -- le -- za_es -- tra -- nha
  que_e -- mer -- giu da_en -- tra -- nha das pla -- gas a -- zuis
  num es -- plen -- dor de gló -- ria a -- vis -- ta -- ram_u'a gran -- de
  luz

  fa -- di -- ga -- do_e far -- to de cla -- mar às pe -- dras
  de pro -- por jus -- ti -- ça_ao mun -- do pe -- ca -- dor

  vô pros -- si -- gui -- no_es -- tra -- da_a fo -- ra
  ru -- mo_à -- estre -- la ca -- no -- ra
  e_ao se -- nhor das Sea -- ras a Je -- sus eu lô -- vo
  le -- vam_os qua -- tro ven -- tos a meu Deus um can -- to novo

}

\score {
  <<
    \new ChordNames {
      \crd
    }

    \new Staff {
      \key d \major
      \time 3/4

      \new Voice \with { \consists "Pitch_squash_engraver" } {
        \melody
      }
    }

    <<
      \new Lyrics \lyricsto "melody" { \letraa }
      \new Lyrics \lyricsto "melody" { \letrab }
      \new Lyrics \lyricsto "melody" { \letrac }
    >>
  >>
}
