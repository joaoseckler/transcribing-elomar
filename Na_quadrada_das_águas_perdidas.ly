\version "2.20.0"
\include "defaults.ly"

\header {
  title = "Na quadrada das águas perdidas"
  composer = "Elomar"
}

solma = <d g b>4
rema = <d fis a>4

mtomais = \markup \column { \italic {
  " " % Hack to increase vertical space of markup
  " "
  "muito mais, inda mais, muito mais..."
}}

\score {
  <<
    \new ChordNames {
      \set chordChanges = ##t
      \chordmode {
        \repeat unfold 36 { a2:7 | }
        \repeat unfold 4 { e2:7 | }
        d2 |
        e:m |
        b:7 |
        e:m |
        d |
        e:m |
        b:7 |
        e:m |
        b:7 |
        e:7 |
        e:7 |
        \repeat unfold 7 { a2:7 | }
        \repeat unfold 3 { d  | b:m |}
        d2 |
        \repeat unfold 6 { e2 |}
        g | g |
        d | d |
        c | c | c | c | d |
        a:m | a:m
        c |
        d |
        e | e | e | e |
      }
    }
    \new Staff {
      \key a \major
      \time 2/4
      \repeat volta 2 {
        \new Voice = "intro" \relative c' {
          a16 e' g a cis e g a b a g e cis a g e |
          r2 | r2 | r2 | r2 | r2 | r2 | r2 | r2 |
        }
        \new Voice = "melodia" \relative c' {
          r4 r16 a cis e |
          g8 g a fis~ |
          fis e cis16 e g8~ |
          g2 |
          r2_\mtomais | r2 | r2 | r2 | r2 | r2 | r2 | r2 | r2 |

          r8 a, cis e |
          g a b16 g a8~ |
          a fis e cis |
          fis e~ e4 |
          r2_\mtomais | r2 | r2 | r2 | r2 | r2 |

          r8 a, cis e |
          g b d b |
          cis a4 fis16 e |
          e2 |
          r2 | r2 | r2 |

          \clef F
          d4 c8 d |
          e4 d8 c b4 a8 b |
          g e~ e4 |
          r8 d' c d |
          e2 |
          b16 b~ b8 a b |
          g b~ b4 |

          \clef G
          r8 dis cis dis |
          e4 e8 e8 |
          gis4 fis8 a |
          a2 |
          r2 | r2 | r2 | r2 | r2 |

          r8 a \tuplet 3/2 { g8 fis e } |
          \repeat unfold 5 {
            \tuplet 3/2 { d4 d8 } \tuplet 3/2 { d d d } |
          }
          \tuplet 3/2 { d8 d d } \tuplet 3/2 { d d d } |
          \tuplet 3/2 { d8 d d } \tuplet 3/2 { d e fis } |
          e2~ | e2 | r2 | r2 | r2 | r2 |

          \tuplet 3/2 {\solma \solma \solma} |
          \tuplet 3/2 { \solma \rema \solma } |
          \tuplet 3/2 { \rema \rema \rema } |
          \tuplet 3/2 { \rema <d e g>4 <d fis> } |
          <c e g>2 |
          <g c g'> |

          r2 |
          \tuplet 3/2 { c8 c c } \tuplet 3/2 { e e e } |
          \tuplet 3/2 { fis fis fis } \tuplet 3/2 { g g fis } |
          e2 |

          r2 |
          \tuplet 3/2 { g8 g g } \tuplet 3/2 { g g a } |
          \tuplet 3/2 { fis fis fis } \tuplet 3/2 { fis e d } |
          e2~ |
          e2 |
          r2 |
          r2
        }
      }
    }

    \new Lyrics \lyricsto "melodia" {
      da ca -- ran -- to -- nha mili lé -- gua_a ca -- mi -- nhá
      da Va -- ca Sê -- ca, Se -- te Var -- ge_in -- da pr'á lá __ _
      % muito mais, inda mais, muito mais
      dis -- pois dos der -- ra -- dê -- ro can -- tão do ser -- tão

      lá na qua -- dra -- da das á -- guas per -- di -- da
      rêis, Mãe\-se -- nhora
      be -- le -- za_is -- qui -- ci -- da
      bens, a la -- go -- a_ar -- ris -- co -- sa fun -- ção

      ôh câin -- do chi -- que -- ra_as ca -- bra mais
      ce -- do_a -- par -- ta_os ca -- br -- ito_mi cu -- ra
      Se -- gre -- do chin -- cha Lu -- bião, esse bo -- de
      mal -- va -- do, tra -- van -- ca_o chi -- quê -- ro
      ti_a -- vi -- a_a cui -- dar

      a -- las qui_as pol -- da di She -- da
      rin -- cha -- ro_ao lu -- á __ _
      na ma -- dru -- ga -- da su -- a -- das de me -- do pr'á lá
      run -- cas le -- van -- do a -- ce -- sas
      can -- dei -- a_in -- lu -- são
    }
    \new Lyrics \lyricsto "melodia" {

      da ca -- ran -- to -- nha mili lé -- gua_a ca -- mi -- nhá
      % muito mais, inda mais, muito mais
      mil ba -- da -- ro -- nha tem que tê pr'á che -- gá lá __ _
      % muito mais, inda mais, muito mais
      Se -- te ji -- ne -- la se -- te sa -- la_um ca -- sa -- rão

      la -- ço dos Mou -- ra
      var -- ge dos tru -- men -- to
      ve -- lhos do -- mingos
      ca -- sa dos Sar -- men -- tos
      mo -- ças, si -- nho -- ras
      mi -- trio -- sa fun -- ção
      dá pres -- sa_in Gui -- lo -- ra_a_ingo -- má
        noss -- os terno
      al -- bar -- da_os ju -- mento cum_as ca -- pa de_in -- verno
      cui -- da_as fer -- ra -- men -- ta num dê -- xa_e -- la vê
      Si não po -- de ela num a -- nu -- i noi i
      On -- te pr'os nor -- te de Mi -- na_o re -- lam -- po
        rai -- ô __ _
      mu -- ca -- dim a mão do ri as á -- gua já to -- mô
      an -- da mun -- te -- mo mon -- den -- go pr'á nois i prá lá
    }
  >>
}


%{


%}

