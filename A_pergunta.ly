\version "2.20.0"

\include "defaults.ly"
 % alinhar as letras com a melodia
 % acordes

\header {
  title = "A Pergunta"
  composer = "Elomar"
}

melody = \relative c' {
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1,1,1
  \time 4/4
  \repeat unfold 7 { r1 }
  r2 b'8. a16 g\trill fis e d |

  \time 3/4
  \set Timing.beatStructure = 1,1,1

  \repeat unfold 2 {
    d4 e8. fis16 gis a gis fis |
  }
    \alternative{{
      e4 b'8. a16 g\trill fis e d |
    }{
      e4.\fermata
    }
  }

  \new Voice = "melody" {
    d'8 c b |
    a^\segno a a gis a gis |
    a4. gis8 a b |
    g g g fis g a |
    e4. d'8 c b |

    a a a gis a gis |
    a4. gis8 a b |
    g g g fis g a |
    e4 \fermata  b'8. a16 g fis e d |
    \repeat unfold 4 {
      d4 e8. fis16 gis a gis fis |
    }
      \alternative{{
        e4 b'8. a16 g fis e d |
      }{
        e2.^\coda |
      }
    }
    d'8 d d d d d |
    d cis d cis d e  |
    d cis b a g a |
    b4. e,8 -\markup {
    \italic "D.S. al Coda"
    } fis gis |

   \break

   \once \override Staff.KeySignature.break-visibility = #end-of-line-invisible
   \once \override Staff.Clef.break-visibility = #end-of-line-invisible
   \once \override Score.RehearsalMark.extra-offset = #'( -4.5 . 0 )

    % The coda
    \time 4/4
    c,8^\coda_\markup{\italic "rubato"} c c c d d e e~ |
    e4 e8 e fis g a g|
    a g fis e d e4.~ |
    e1 |
    c8 c c c d d e e~ |
    e2 e8 e fis g |
    a g a g fis e d a'~ |
    a4 b2 aes8 b |
    \time 9/8
    \set Timing.beatStructure = 5,4
    <e c> <d b> <c a> <b g> <a f> <g e> <a f> <b g> <c a> |
    \time 4/4
    b2. r8 b |
    a4 a8 a16 a16 a8 a a a |
    a2. r8 b |
    g8. g16 g8 fis g g g a |
    g2. r8 b |
    a8. a16 a8 a a a a a |
    a2. r8 a |
    g8 fis e fis g a g a |
    e2 \fermata  b'8. a16 g fis e d |
    \time 3/4
    \repeat unfold 2 {
      d4 e8. fis16 gis a gis fis |
    }
      \alternative{{
        e4 b'8. a16 g fis e d |
      }{
        e2. |
      }
    }
    \bar"|."
  }
}

crd = \chordmode {
  \set chordChanges = ##t
  \time 4/4
  a1:m | a:m | g | g |
  a1:m | a:m | e:m | e:m |
  \time 3/4
  d2. | e | d | e |
  d | a:m | g | e:m |
  d | a:m | g | e:m |
  \repeat unfold 4 { d | }
  \alternative { { e | } { e | } }
  b:m | b:m | b:m | g |
  \time 4/4
  \repeat unfold 7 { a1:m | }
  a4 e2. |
  \time 9/8
  c1~ c8 |
  \time 4/4
  g1 |
  a:m | a:m |
  g | g |
  a:m | a:m |
  e:m | e:m |
  \time 3/4
  d2. | e | e
}

letraa = \lyricmode
{
  Ô Qui -- li -- me -- ro_As -- sun -- ta meu ir -- mão
  I -- an -- tes mêr -- mo que nóis dois sau -- demo
  Eu te pre -- gun -- to na -- que -- le re -- frão
  Qui na far -- tu -- ra nóis sem -- pre can -- temo

  Na ca -- tin -- ga tá chu -- veno
  Ri -- bei -- rão is -- tão in -- cheno
  Na ca -- tin -- ga tá chu -- veno
  Ri -- bei -- rão is -- tão in -- cheno
  Me_ar -- res -- pon -- da meu ir -- mão
  Cu -- ma_o po -- vo de lá tão
  _ _ _ _ _ _ _ _ _ _ _ _ _ _

  Só a ter -- ra que vo -- cê de -- xo
  Quin -- da tá lá num ri -- ti -- rou\-se não
  Os po -- vo_as


  Vai prá mais de du -- as lu -- a
  Que meu pai man -- dô eu i no Na __ _  -- za -- ré
  Bus -- cá_u'a quar -- ta de fa -- ri -- nha
  Eu e_o_ir -- mão Zé Ben -- to vi -- nha an -- da -- no_a pé __ _

  Mã -- e lu -- a ma -- gri -- nha qui es -- tá no céu
  Se -- rá qui cua -- no_eu che -- go_in mi -- nha terra
  A -- in -- da vou_en -- con -- trar o que é meu
  Se -- rá que Deus do céu aqui na ter -- ra
  De nos -- so po -- vo_in -- ton -- ce se_is -- que -- ceu


  Na ca -- tin -- ga mor -- reu tudo
  Qui nem pre -- ci -- so ca -- xão
  Meu cum -- pa -- dre João Bar -- budo
  Num cum -- priu o -- bri -- ga -- ção
}

letrab = \lyricmode {
  _ _ _ gen -- te_os bi -- cho_as coi -- sa tudo
  Uns ri -- ti -- rou\-se_in pi -- ri -- gri -- na -- ção
  Os o -- tro_os mais ve -- lho mais ca -- bi -- çudo
  Vol -- ta -- ro pru qui_e -- ra pru pó do chão
  A -- dis -- pois de cu -- mê tudo
  Cu -- mêr pre -- ca -- ta sur -- rão
  Cu -- mêr co -- ro de ra -- budo
  Cu -- mêr cu -- ru -- ru ro -- dão
  _ _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _ _

  E_as ca -- cim -- ba do ri ga -- vi -- ão
  Já deu mais de du -- as cova d'um cris -- tão
  In -- té a
}

letrac = \lyricmode {
  _ _ _ que -- la a da ca -- ra fêa
  Se ve -- no só de -- xô a ter -- ra_a -- lêa
  Foi nas pi -- dri -- nha co -- va de se -- rêa
  Vê sua ma -- dri -- nha_e vei de mão c'ua vea

  Ai dis -- pois di co -- mer tudo
  Fo -- ram rir co -- me -- ram_a -- veia
  Cu -- meram cô -- ro de ra -- budo
  Ca -- pa de can -- ga -- ia véia
  Na ca -- tin -- ga mor -- reu tudo
  Qui nem pre -- ci -- so ca -- xão
  Meu cum -- pa -- dre João Bar -- budo
  Num cum -- priu o -- bri -- ga -- ção


}

\score {
  <<
    \new ChordNames {
      \crd
    }

    \new Staff {
      \key a \minor

      \new Voice \with { \consists "Pitch_squash_engraver" } {
        \melody
      }
    }

    <<
      \new Lyrics \lyricsto "melody" { \letraa }
      \new Lyrics \lyricsto "melody" { \letrab }
      \new Lyrics \lyricsto "melody" { \letrac }
    >>
  >>
}
