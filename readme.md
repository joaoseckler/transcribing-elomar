### Transcriptions

of songs by Elomar, mostly in the album "Na quadrada das águas perdidas".
About the transcription software, see [lilypond](lilypond.org).

#### How to compile
`$ make`

#### Dependencies
- make
- lilypond

