(G C Em C/E Em6 Em7 Em C Em)
(A Em A Em)
(A Em A Em)

G
Já qui tu vai lá prá fêra
      Em
Traga di lá para mim
G
Agua do fulô qui chêra
                  C
Um nuvelo e um carmim
                   Em
Trais um pacote de misse
                   C/E
Meu amigo ah se tu visse
                 Em6
Aquele cego cantadô!
               Em7
Um dia ele me disse
                   Em7+  Em
Jogano um mote de amô
                   C
Qui eu havéra de vivê
         Em
Pur esse mundo

E morrê ainda em flô

(A Em A Em)

G
Passa naquela barraca
     Em
Daquela mulé reizêra
G
Onde almuçamo paca
                C
Panelada e frigidêra
                    Em
Inté você disse uma lõa
              C/E
Gabano a boia bôa
                  Em6
Qui das casa da cidade
                Em7   Em
Aquela era a primêra
                        C
Trais pra mim vãs brividade
             Em
Qui eu quero matá a sôdade
                      G     C G
Fais tempo qui fui na fêra
Em   A Em
Ai sôdade...

(Em A Em A Em)

G
Apois sim vê se num isquece
       Em
Quinda nessa lua chêa
G
Nós vai brincá na quermesse
                 C
Lá no Riacho d'Arêa
                Em
Na casa daquêle home
                C/E
Feitecêro e curadô
                   Em6
Que o dia intêro é home
                  Em7
Filho do Nosso Sinhô
                    Em7+
Mais dispois da mêa noite
               Em
É lubisome cumedô

Dos pagão qui as mãe isqueceu
                G  C G
Do batismo salvadô
Em
E tem mais dois garrafão
                         D
Cum dois canguin responsadô


G
Apois sim vê se num isquece
      Em
De trazê ruge e carmim
G
Ah se o dinheiro desse!
                   C
Eu quiria um trancilin
                      Em
E mais treis metro de chita
                      C/E
Qui é preu fazê um vistido
                   Em6
E ficá bem mais bunita
                 A
Qui Madô de Juca Dido
                  B
Qui Zefa de lô Joaquim
G
Já qui tu vai lá prá fêra

Meu amigo trais
                     G    C  G
Essas coisinhas para mim...
G
Já qui tu vai lá prá fêra

Meu amigo trais
                     Em
Essas coisinhas para mim...

(Em A Em A)

