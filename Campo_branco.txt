Campo branco minhas penas que pena secou
todo o bem qui nois tinha era chuva era o amor
num tem nada não nois dois vai penano assim
campo lindo ai qui tempo ruim
tu sem chuva e a tristeza em mim
peço a Deus a meu Deus grande Deus de Abraão
prá arrancar as penas do meu coração
dessa terra sêca in ança e aflição
todo bem é de Deus qui vem
quem tem bem lôva a Deus seu bem
quem não tem pede a Deus qui vem
pela sombra do vale do ri Gavião
os rebanhos esperam a trovoada chover
num tem nada não também no meu coração
vô ter relampo e trovão
minh'alma vai florescer
quando a amada e esperada trovoada chegá
iantes da quadra as marrã vão tê
sei qui inda vô vê marrã pari sem querer
amanhã no amanhecer
tardã mais sei qui vô ter
meu dia inda vai nascer
e esse tempo da vinda tá perto de vin
sete casca aruêra cantaram prá mim
tatarena vai rodá vai botá fulô
marela du u'a veis só
prá ela de u'a veis só.
