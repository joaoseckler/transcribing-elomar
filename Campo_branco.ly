\version "2.20.0"
\include "defaults.ly"

\header {
  title = "Campo branco"
  composer = "Elomar"
}

staffglobal = {
  \key c \minor
  \time 2/4
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1
  \partial 16*3
}

smallfont = #-4

tema = \relative c''' {
  bes16 a g |
  g8 f es d |
  es c d es16 f |
  es8 c b g |
  c c16 d d8 es16 f |
  es8 c b g |
  c2 |
}

introm = \relative c'' {
  \tema

  r4 es8 f |
  g4 f8 es |
  f4 es8 d |
  es4 d8 f |
  c r g' a |
  bes4 aes?8 g |
  aes4 g8 f |
  g4 f8 es |
  f4 r |
  g8 f es d |
  c2 |
  c2 |
  c4 e |
  d e8 d |
  c2 |
}

mela = \relative c' {
  r4 es8 f |
  g4 f8 es |
  f4 es8 d |
  es4 d8 f |
  c r g' a |
  bes4 aes?8 g |
  aes4 g8 f |
  g4 f8 es |
  f4 r |
  g8 f es d |
  c2 |
  c2 |
  c4 e8 c |
  d4 d |
  c2 |

  \once \override Score.RehearsalMark.font-size = #2
  \mark \markup { \musicglyph #"scripts.coda" }
  r4 c8 e |
  g4 g8 b |
  a4 a |
  e2 |
  c4 c |
  es4 f8 g |
  f4 f |
  c4 r16
}

melb = \relative c' {
  r4 es8 f |
  g4 f8 es |
  f4 es8 d |
  es4 d8 es |
  bes2 |
  c8 d es f |
  es c~ c4 |
  c8 d es c |
  bes g~ g4 |
  c8 c d es |
  f4 g8 f |
  es d c bes |
  c2 |

  c4 c |
  c e8 c |
  d4 d |
  c2 |
  r4 c8 e |
  g4 g8 b |
  a4 a |
  e2 |
  c4 c |
  es4 f8 g |
  f4 f |
  c4 r16
}

finalvoz = \relative c' {
  \repeat volta 2 {
    \once \override Score.RehearsalMark.font-size = #3
    \mark \markup { \musicglyph #"scripts.coda" }
    r4 e |
    g a8 b |
    a4 a |
    e2 |
    r4 c |
    es? f8 g |
    f4 f |
  }
  \alternative {
    { c2 }
    { c4 r16 }
  }
}

finalflauta = \relative c'' {
  \tema
  r4 es8 f |
  g4 f8 es |
  f4 es8 d |
  es4 d8 f |
  c2 |

}

temacrd = \chordmode {
  c2:m |
  c2:m |
  c4:m g:7 |
  c2:m |
  c4:m g:7 |
  c2:m
}

aacrd = \chordmode {
  s2
  es |
  bes |
  aes |
  c:m |
  bes |
  f:m |
  es |
  f:m |
  bes8 es bes es |
  aes2 |
  c |
  a:m |
  d:m |
  c |
}

abcrd = \chordmode {
  s2 |
  e:m/g |
  a:m |
  e:m/g |
  aes |
  es |
  f4:m f |
  c2 |
}

bcrd = \chordmode {
  c2:m |
  es |
  bes |
  aes4 a:dim |
  bes2:7 |
  c:m |
  c:m |
  c:m |
  g:m |
  c:m |
  f4:m f |
  bes8 es bes es |
  aes2 |
  c |
  a:m |
  d:m |
  c |
  \abcrd
}

introcrd = \chordmode {
  s8.
  \temacrd
  \aacrd
}

finalcrd = \chordmode {
  c2:m |
  \temacrd
  c2:m |
  es2 |
  bes |
  aes |
  c:m
}


\score {
  <<
    \new ChordNames \chordmode {
      \set chordChanges = ##t
      \introcrd
      \aacrd
      \abcrd
      \temacrd
      \bcrd
      \temacrd
      \abcrd
      \finalcrd
    }

    \new Staff {
      \staffglobal

      \new Voice = "intro"
      \with { fontSize = \smallfont } {
        \introm
      }

      \repeat volta 2 {
        \new Voice = "melodiaA" \relative c'' {
          \mela
        }
        \new Voice
        \with { fontSize = \smallfont } {
          \tema
        }
        \new Voice = "melodiaB" \relative c'' {
          \melb
        }
        \new Voice
        \with { fontSize = \smallfont } {
          \tema
        }
      }

      \break
      \new Voice = "coda"
      {
        \finalvoz
      }
      \new Voice
      \with { fontSize = \smallfont } {
        \finalflauta
      }
      \bar "||"

    }

    \new Lyrics \lyricsto "melodiaA" {
      cam -- po bran -- co minhas pe -- nas que pe -- na se -- cou
      to -- do_o bem qui nois ti -- nha_e -- ra
        chu -- va_e -- ra_o_a -- mor
      num tem na -- da não nois dois vai pe -- na -- no_as -- sim
      cam -- po lin -- do_ai qui tem -- po ruim
      tu sem chu -- va_e_a tris -- te -- za_em mim
    }
    \new Lyrics \lyricsto "melodiaA" {
      pe -- la som -- bra do va -- le do ri Ga -- vi -- ão
      os re -- ba -- nhos es -- pe -- ram_a tro -- voa -- da cho -- ver
      num tem na -- da não tam -- bém no meu co -- ra -- ção
      vô ter re -- lam -- po e tro -- vão
      mi __  _ -- nh'a -- lma vai flo -- res -- cer
    }
    \new Lyrics \lyricsto "melodiaA" {
      e_es -- se tem -- po da vin -- da tá per -- to de vin
      se -- te cas -- ca_a -- ru -- ê -- ra can -- ta -- ram prá mim
      ta -- ta -- re -- na vai ro -- dá vai bo -- tá fu -- lô
    }
    \new Lyrics \lyricsto "melodiaB" {
      pe -- ço_a Deus a meu Deus gran -- de Deus de_A -- bra -- ão
      prá_ar -- ran -- car as pe -- nas do meu co -- ra -- ção _
      des -- sa ter -- ra sê -- ca in an -- ça e_a -- fli -- ção
      to -- do bem é de Deus qui vem
      quem tem bem lô -- va_a Deus seu bem
      quem não tem pede_a Deus qui vem

    }
    \new Lyrics \lyricsto "melodiaB" {
      quan -- do_a_a -- ma -- da_e_es -- pe -- ra -- da
        tro -- voa -- da che -- gá
      i -- an -- tes da qua -- dra as mar -- rã vão tê _
      sei qui_in -- da vô vê mar -- rã pa -- ri sem que -- rer
      a -- ma -- nhã no a -- ma -- nhe -- cer
      tar __ _ dã mais sei qui vô ter
      meu __ _ di -- a_in -- da vai nas -- cer
    }

    \new Lyrics \lyricsto "coda" {
      ma -- re -- la du u'a veis só
      prá e -- la de u'a veis só
      só % repeat hack!
    }
  >>
}


