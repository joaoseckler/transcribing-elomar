\version "2.20.0"

\header {
  tagline = ""
}

\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 5)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 5)
       (stretchability . 12))
}

\layout {
  \context {
    \Lyrics
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #1.5
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #1.5
      \override LyricSpace.minimum-distance = #1.0
  }
  \context {
    \ChordNames
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #2
    \override LyricSpace.minimum-distance = #1.0
  }
}

% Write comoveu\-me instead of comoveu -- -me (which gives an error)
"\\-" = \tweak minimum-distance #1 #(make-music 'HyphenEvent)

% put this somewhere:
% \set majorSevenSymbol = \markup { maj7 }
