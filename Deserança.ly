\version "2.20.0"

\include "defaults.ly"

\header {
  title = "Deserança"
  composer = "Elomar"
}

melody = {
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1,1
  \relative c' {
    \time 4/8
    fis8_"capotraste III" fis fis fis |
    \time 3/8
    g g g |
    d d e |
    f f f |
    bes, bes c |
    d d d |
    f f a |
    bes2. |
    \time 4/8 |
    g8 g g a |
    \time 3/8
    bes bes bes |
    g g bes |
    \time 3/4
    \set Timing.beatStructure = 1,1,1
    c c c b c c |
    \time 3/8
    d d d |
    c bes a |
    g f es |
    \time 4/8
    d es d es |
    \time 2/4
    d2~ |
    d4.
  }

  \new Voice = "melody" {
    \relative c' {
      d8 |
      g,2 |
      bes8 d g16 a bes a |
      d,2 |
      d8 g a16 d f d |
      b8 g~ g8. b16 |
      a8 a8~ a16 a a b |
      fis8 fis~ fis a16 fis |
      f?8 f16 f~ f8 g |
      e4 \tuplet 3/2 { e8 f e } |
      d8. d16~ d8 d |
      d2 |
      r4. d8 |
      g,4~ g8 bes16 d~ |
      d g8. \tuplet 3/2 { a8 bes a } |
      bes,2 |
      \tuplet 3/2 {bes8 des f} \tuplet 3/2 {bes8 des bes} |
      g8 es~ es4 |
      f16 f f8~ f16 f f f |
      d8. d16 f es d8~ |
      d8 d~ \tuplet 3/2 { d8 d d } |
      d8. d16~ d8 d16 d |
      d8. d16~ \tuplet 3/2 { d8 d d } |
      d4. d16 d |
      d8 d b b16 d~ |
      d4~ d8 fis16 d |
      e8. e16 e e8 d16~ |
      d8. d16~ d4 |
      \tuplet 3/2 {d8 d d} \tuplet 3/2 {d8 d d} |
      d8. d16~ d8 d16 d |
      d8 d16 d~ d d8 c16~ |
      c2 |
      r4. g'8 |
      c,4~ \tuplet 3/2 { c8 es g } |
      c4~ c16 d es d |
      es,8. es16~ es4 |
      \tuplet 3/2 { es ges bes } \tuplet 3/2 { es ges es } |
      c8. aes16~ aes8. bes16 |
      bes8 bes16 c~c bes bes c |
      g4~ g16 g bes g |
      d8 d \tuplet 3/2 { d8 f? d } |
      d4~ d8 \tuplet 3/2 { d16 d d } |
      d8. d16~ \tuplet 3/2 { d8 d d } |
      d4 r16 d d d |
      d d8 b16~ b16 b8 d16~
      d4 r16 fis fis fis |
      e8. e16 \tuplet 3/2 {e8 e e} |
      d4~ d16 fis fis fis |
      gis8 gis16 e~ e e e fis~ |
      fis8 fis16 a a8 a16 gis~ |
      gis8 e16 e e e8 e16 |
      fis8. fis16 \tuplet 3/2 { fis8 fis fis  } |
      \tuplet 3/2 { g g g } \tuplet 3/2 { d d e } |
      \tuplet 3/2 { f f f } \tuplet 3/2 { bes, bes c } |
      \tuplet 3/2 { d d d } \tuplet 3/2 { f f a } |
      bes4 g16 g g a |
      \tuplet 3/2 { bes8 bes bes } \tuplet 3/2 { g g bes } |
      \tuplet 3/2 { c c c } \tuplet 3/2 { b c c } |
      \tuplet 3/2 { d d d } \tuplet 3/2 { c bes a } |
      g4
    }
  }
  \relative c'' {
    g8 bes~ |
    bes c4 d8~ |
    d c4 bes8~ |
    bes8 a4 g8~ |
    g8 g4 bes8~ |
    bes c4 d8~ |
    d c4 bes8~ |
    bes8 a4 g8~ |
    g2 |
  }
}


crd = \chordmode {
  \set majorSevenSymbol = \markup { maj7 }
  \time 4/8
  gis2:m |
  \time 3/8
  e4.:m |
  g4 a8 |
  d4. |
  g4 a8 |
  b4.:m |
  b4:m d8:7 |
  e4.:m |
  s4. |
  \time 4/8
  c4. d8 |
  \time 3/8
  e4.:m |
  c4 e8:m |
  \time 3/4
  a2. |
  \time 3/8
  b4.:7 | 
  a8 g d |
  c g a:m |
  \time 4/8
  g a:m g a:m |
  \time 2/4
  b2:7 |
  s |
  e:m |
  s |
  b:m |
  s |
  e |
  fis |
  b |
  d |
  a |
  g |
  fis:7sus4 |
  s |
  e:m |
  s |
  g:m |
  s |
  c |
  d |
  g |
  b:m |
  e:m |
  e:m7+ |
  e:m7 |
  \set additionalPitchPrefix = #"add"
  e:5.9 |
  gis:m |
  cis |
  gis:m |
  g |
  b:m |
  e:m |
  a:m |
  s |
  s |
  s |
  c: m |
  s |
  s |
  f |
  g |
  c |
  b |
  g |
  b:m |
  e:m |
  e:5.9 |
  gis:m |
  cis |
  gis:m |
  cis |
  gis:m |
  cis |
  gis:m |
  e4:m g8 a |
  d4 g8 a |
  b4.:m d8:7 |
  e4:m c8. d16 |
  e4:m c8. e16:m |
  a2 |
  b4  \tuplet 3/2 { a8 g d } |
  e4.:m g8 |
  s a4 b8 |
  s a4 g8 |
  s8 d4 e8 |
  s e4:m g8 |
  s a4 b8 |
  s a4 g8 |
  s8 d4 e8:m |
  s2 |




















}

letra = \lyricmode
{
  Já não sei mais o que_é fa -- zer contas
  a -- té já per -- di as con -- tas
  dos can -- tos dos rios das con -- tas
  que meu pei -- to_a -- mor, can -- tou
  per -- di -- do de_a -- mor por ti
  já nem me lem -- bro quan -- tas can -- tigas
  quan -- tas ti -- ra -- nas a -- mi -- ga
  na vi -- o -- la pa -- de -- ci
  tam -- bém não sei mais quan -- tos fo -- ram
  os lua -- res que pas -- sa -- ram
  pe -- lo vão des -- sa ja -- nela
  in -- da -- gan -- do su -- pli -- can -- tes
  fri -- os, pá -- li -- dos, de -- men -- tes,
  on -- de an -- da_a_a -- mi -- ga_a -- quela
  vi -- es -- te de lon -- ge_e -- ras tão lin -- da
  co -- mo se_ho -- je lem -- bro_a -- in -- da
  a man -- si -- tu -- de da ma -- nhã
  foi tu -- a vin -- da a -- mi -- ga vã
  doi\-me no pei -- to_ao re -- lem -- brar
  já não tem jei -- to_a vi -- da_é vã
  qui di -- se -- ran -- ça ó mi -- nha_ir -- mã
  mas a -- pe -- sar de tu -- do des -- feito
  de tan -- to sonho mor -- to qui num tem mais jeito
  tom -- ban -- do_a la -- dei -- ra
  já pe -- la des -- ci -- da
  na tar -- de da vi -- da
  rom -- po sa -- tis -- feito
  fos -- te na jor -- na -- da_a
  jor -- na -- da per -- di -- da
  meu a -- mor pre -- té -- ri -- to mais que per -- feito
}


\score {
  <<
    \new ChordNames {
      \crd
    }

    \new Staff {
      \key g \minor

      \new Voice \with { \consists "Pitch_squash_engraver" } {
        \transpose g e' { \melody }
      }
    }
    \new Lyrics \lyricsto "melody" { \letra }
  >>
}
